package pl.app.data.loader;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import pl.app.TestConfig;

import java.util.List;
import java.util.Map;

/**
 * Created by Kuba on 08.09.2020.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = TestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class DatabaseDataLoaderTest {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void shouldAutoInitDatabaseWithDataFromCsv() {
        //then
        List<Map<String, Object>> resultList = jdbcTemplate.queryForList("SELECT * FROM DATA");
        Assertions.assertThat(resultList).hasSize(23198);
    }
}