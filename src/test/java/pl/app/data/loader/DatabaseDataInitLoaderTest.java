package pl.app.data.loader;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import pl.app.TestConfig;
import pl.app.data.extractor.ColumnDataInformation;
import pl.app.data.extractor.DataType;
import pl.app.data.extractor.ExtractedData;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by Kuba on 08.09.2020.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = TestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class DatabaseDataInitLoaderTest {

    @Autowired
    private DatabaseDataLoader databaseDataLoader;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Before
    public void init() {
        jdbcTemplate.execute("DROP TABLE DATA");
    }

    @Test
    public void shouldAddDataToDatabase() {
        //given
        List<ColumnDataInformation> columnDataInformationList = Arrays.asList(
                new ColumnDataInformation("Datasource", DataType.TEXT),
                new ColumnDataInformation("Campaign", DataType.TEXT),
                new ColumnDataInformation("Daily", DataType.DATE),
                new ColumnDataInformation("Clicks", DataType.INTEGER),
                new ColumnDataInformation("Impressions", DataType.INTEGER)
        );
        List<List<String>> records = new ArrayList<>();
        records.add(Arrays.asList("Google Ads", "Adventmarkt Touristik", "11/12/19", "7", "22425"));

        //when
        databaseDataLoader.loadDataToDataBase(new ExtractedData(columnDataInformationList, records));

        //then
        List<Map<String, Object>> resultList = jdbcTemplate.queryForList("SELECT * FROM DATA");
        Assertions.assertThat(resultList).hasSize(1);
        Map<String, Object> result = resultList.get(0);
        Assertions.assertThat(result.get("DATASOURCE")).isEqualTo("Google Ads");
        Assertions.assertThat(result.get("CAMPAIGN")).isEqualTo("Adventmarkt Touristik");
        Assertions.assertThat(result.get("DAILY")).isEqualTo(Date.valueOf(LocalDate.of(2019, 11, 12)));
        Assertions.assertThat(result.get("CLICKS")).isEqualTo(7);
        Assertions.assertThat(result.get("IMPRESSIONS")).isEqualTo(22425);
    }
}