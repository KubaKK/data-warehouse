package pl.app.data.extractor;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Kuba on 08.09.2020.
 */
public class CsvDataExtractorTest {
    @Test
    public void shouldExtractCsvData() {
        //given
        CsvDataExtractor objectUnderTests = new CsvDataExtractor();

        //when
        ExtractedData result = objectUnderTests.extractData();

        //then
        //@formatter:off
        List<ColumnDataInformation> expectedResult = Arrays.asList(
                new ColumnDataInformation("Datasource", DataType.TEXT),
                new ColumnDataInformation("Campaign", DataType.TEXT),
                new ColumnDataInformation("Daily", DataType.DATE),
                new ColumnDataInformation("Clicks", DataType.INTEGER),
                new ColumnDataInformation("Impressions", DataType.INTEGER)
        );
        //@formatter:on
        Assertions.assertThat(result.getColumnDataInformationList()).isEqualTo(expectedResult);
        Assertions.assertThat(result.getRecords()).hasSize(23198);
    }
}