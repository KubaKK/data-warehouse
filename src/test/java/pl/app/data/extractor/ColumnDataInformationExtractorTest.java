package pl.app.data.extractor;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Kuba on 08.09.2020.
 */
public class ColumnDataInformationExtractorTest {
    @Test
    public void shouldExtractDateColumnInformation() {
        //given
        ColumnDataInformationExtractor objectUnderTests = new ColumnDataInformationExtractor();
        String columnName = "date";
        List<String> dataToExtractTypeOf = Arrays.asList("11/22/19", "04/22/19");

        //when
        ColumnDataInformation result = objectUnderTests.extractDataColumnInformation(columnName, dataToExtractTypeOf);

        //then
        ColumnDataInformation expectedResult = new ColumnDataInformation(columnName, DataType.DATE);
        Assertions.assertThat(result).isEqualTo(expectedResult);

    }

    @Test
    public void shouldExtractDecimalColumnInformation() {
        //given
        ColumnDataInformationExtractor objectUnderTests = new ColumnDataInformationExtractor();
        String columnName = "decimal";
        List<String> dataToExtractTypeOf = Arrays.asList("10.123", "100.00");

        //when
        ColumnDataInformation result = objectUnderTests.extractDataColumnInformation(columnName, dataToExtractTypeOf);

        //then
        ColumnDataInformation expectedResult = new ColumnDataInformation(columnName, DataType.DECIMAL);
        Assertions.assertThat(result).isEqualTo(expectedResult);

    }

    @Test
    public void shouldExtractIntegerColumnInformation() {
        //given
        ColumnDataInformationExtractor objectUnderTests = new ColumnDataInformationExtractor();
        String columnName = "integer";
        List<String> dataToExtractTypeOf = Arrays.asList("100", "256");

        //when
        ColumnDataInformation result = objectUnderTests.extractDataColumnInformation(columnName, dataToExtractTypeOf);

        //then
        ColumnDataInformation expectedResult = new ColumnDataInformation(columnName, DataType.INTEGER);
        Assertions.assertThat(result).isEqualTo(expectedResult);

    }

    @Test
    public void shouldExtractTextColumnInformation() {
        //given
        ColumnDataInformationExtractor objectUnderTests = new ColumnDataInformationExtractor();
        String columnName = "text";
        List<String> dataToExtractTypeOf = Arrays.asList("test", "lorem ipsum");

        //when
        ColumnDataInformation result = objectUnderTests.extractDataColumnInformation(columnName, dataToExtractTypeOf);

        //then
        ColumnDataInformation expectedResult = new ColumnDataInformation(columnName, DataType.TEXT);
        Assertions.assertThat(result).isEqualTo(expectedResult);

    }

    @Test
    public void shouldExtractTextColumnInformationWhenMultipleTypesInColumn() {
        //given
        ColumnDataInformationExtractor objectUnderTests = new ColumnDataInformationExtractor();
        String columnName = "text";
        List<String> dataToExtractTypeOf = Arrays.asList("11/22/19", "10.123", "123");

        //when
        ColumnDataInformation result = objectUnderTests.extractDataColumnInformation(columnName, dataToExtractTypeOf);

        //then
        ColumnDataInformation expectedResult = new ColumnDataInformation(columnName, DataType.TEXT);
        Assertions.assertThat(result).isEqualTo(expectedResult);

    }
}