package pl.app.data.query;

import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import pl.app.TestConfig;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Created by Kuba on 08.09.2020.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = TestConfig.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class DataFetchingServiceTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DataFetchingService objectUnderTests;

    @Test
    public void shouldReturnDailyImpressions() {
        //given
        List<String> metrics = Arrays.asList("SUM(IMPRESSIONS)", "DAILY");
        List<String> groupBy = Arrays.asList("DAILY");

        //when
        List<String> result = objectUnderTests.fetchData(metrics, Optional.of(groupBy), Optional.empty());

        //then
        Assertions.assertThat(result).hasSize(410);
    }

    @Test
    public void shouldReturnCTRPerDatasourceAndCampaign() {
        //given
        List<String> metrics = Arrays.asList("(1.0*sum(CLICKS)/sum(IMPRESSIONS))*100 as CTR", "Datasource", "Campaign");
        List<String> groupBy = Arrays.asList("Datasource", "Campaign");

        //when
        List<String> result = objectUnderTests.fetchData(metrics, Optional.of(groupBy), Optional.empty());

        //then
        Assertions.assertThat(result).hasSize(185);
    }

    @Test
    public void shouldReturnTotalClicksForDatasourceInDateRange() {
        //given
        List<String> metrics = Arrays.asList("sum(CLICKS) as TOTAL_CLICKS", "Datasource");
        List<String> groupBy = Arrays.asList("Datasource");
        List<String> filters = Arrays.asList("DAILY > '2019-11-13'", "DAILY < '2019-11-15'");

        //when
        List<String> result = objectUnderTests.fetchData(metrics, Optional.of(groupBy), Optional.of(filters));

        //then
        Assertions.assertThat(result).hasSize(3);

    }
}