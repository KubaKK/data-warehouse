package pl.app.data.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import pl.app.data.extractor.ColumnDataInformation;
import pl.app.data.extractor.DataType;
import pl.app.data.extractor.ExtractedData;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Kuba on 08.09.2020.
 */
@Service
public class DatabaseDataLoader {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DatabaseDataLoader(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    void loadDataToDataBase(ExtractedData extractedData) {
        createTable(extractedData.getColumnDataInformationList());
        insertData(extractedData);
    }

    private void insertData(ExtractedData records) {
        DataBatchStatement batchPreparedStatementSetter = new DataBatchStatement(records);
        jdbcTemplate.batchUpdate("INSERT INTO DATA VALUES (?, ?, ?, ?, ?)", batchPreparedStatementSetter);
    }



    private void createTable(List<ColumnDataInformation> columnDataInformationList) {
        StringBuilder createTableQueryBuilder = new StringBuilder();
        createTableQueryBuilder.append("CREATE TABLE DATA ");
        createTableQueryBuilder.append("(");
        String columnsDefinition = columnDataInformationList.stream()
                .map(this::createColumnsDefinition)
                .collect(Collectors.joining(","));
        createTableQueryBuilder.append(columnsDefinition);
        createTableQueryBuilder.append(")");
        jdbcTemplate.execute(createTableQueryBuilder.toString());


    }

    private String createColumnsDefinition(ColumnDataInformation columnDataInformation) {
        return String.format("%s %s NOT NULL",
                columnDataInformation.getColumnName().toUpperCase(), createSQLType(columnDataInformation.getDataType()));
    }

    private String createSQLType(DataType dataType) {
        if (dataType == DataType.DATE) {
            return "DATE";
        }
        if (dataType == DataType.INTEGER) {
            return "INT";
        }
        if (dataType == DataType.DECIMAL) {
            return "DECIMAL(5,2)";
        }
        return "VARCHAR(100)";
    }
}
