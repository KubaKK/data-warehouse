package pl.app.data.loader;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import pl.app.data.extractor.ColumnDataInformation;
import pl.app.data.extractor.DataType;
import pl.app.data.extractor.ExtractedData;

import java.io.Serializable;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Locale;

/**
 * Created by Kuba on 08.09.2020.
 */
public class DataBatchStatement implements BatchPreparedStatementSetter, Serializable {

    private final ExtractedData records;

    DataBatchStatement(ExtractedData records) {
        this.records = records;
    }

    @Override
    public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
        List<String> rowValues = records.getRecords().get(i);
        for (int j = 0; j < rowValues.size(); j++) {
            String rowValue = rowValues.get(j);
            ColumnDataInformation columnDefinition = records.getColumnDataInformationList().get(j);
            int dbColumnIndex = j + 1;
            if (columnDefinition.getDataType() == DataType.INTEGER) {
                preparedStatement.setInt(dbColumnIndex, NumberUtils.toInt(rowValue));
            } else if (columnDefinition.getDataType() == DataType.DECIMAL) {
                preparedStatement.setDouble(dbColumnIndex, NumberUtils.toDouble(rowValue));
            } else if (columnDefinition.getDataType() == DataType.DATE) {
                LocalDate date = LocalDate.parse(rowValue, DateTimeFormatter.ofPattern("MM/dd/yy", Locale.ENGLISH));
                preparedStatement.setDate(dbColumnIndex, Date.valueOf(date));
            } else {
                preparedStatement.setString(dbColumnIndex, rowValue);
            }
        }
    }

    @Override
    public int getBatchSize() {
        return records.getRecords().size();
    }
}
