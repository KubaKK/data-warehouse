package pl.app.data.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.app.data.extractor.CsvDataExtractor;
import pl.app.data.extractor.ExtractedData;
import pl.app.data.loader.DatabaseDataLoader;

import javax.annotation.PostConstruct;

/**
 * Created by Kuba on 08.09.2020.
 */
@Service
public class DataInitLoader {

    private final DatabaseDataLoader databaseDataLoader;

    @Autowired
    public DataInitLoader(DatabaseDataLoader databaseDataLoader) {
        this.databaseDataLoader = databaseDataLoader;
    }

    @PostConstruct
    public void init() {
        CsvDataExtractor csvDataExtractor = new CsvDataExtractor();
        ExtractedData extractedData = csvDataExtractor.extractData();
        databaseDataLoader.loadDataToDataBase(extractedData);
    }
}
