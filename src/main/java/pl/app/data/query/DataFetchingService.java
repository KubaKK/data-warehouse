package pl.app.data.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Kuba on 08.09.2020.
 */
@Service
public class DataFetchingService {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public DataFetchingService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<String> fetchData(List<String> metrics, Optional<List<String>> groupBy, Optional<List<String>> filterOn) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT ");
        stringBuilder.append(metrics.stream().map(String::toUpperCase).collect(Collectors.joining(",")));
        stringBuilder.append(" FROM DATA");
        if (filterOn.isPresent()) {
            addFilters(filterOn, stringBuilder);
        }
        if (groupBy.isPresent()) {
            addGroups(groupBy, stringBuilder);
        }
        List<Map<String, Object>> queryResult = jdbcTemplate.queryForList(stringBuilder.toString());
        return queryResult.stream().map(this::createResult).collect(Collectors.toList());
    }

    private void addGroups(Optional<List<String>> groupBy, StringBuilder stringBuilder) {
        stringBuilder.append(" GROUP BY ");
        stringBuilder.append(groupBy.get().stream().map(String::toUpperCase).collect(Collectors.joining(",")));
    }

    private void addFilters(Optional<List<String>> filterOn, StringBuilder stringBuilder) {
        stringBuilder.append(" WHERE ");
        stringBuilder.append(filterOn.get().stream().map(String::toUpperCase).collect(Collectors.joining(" AND ")));
    }

    private String createResult(Map<String, Object> resultMap) {
        Set<String> strings = resultMap.keySet();
        return strings.stream().map(key -> String.format("%s: %s", key, resultMap.get(key))).collect(Collectors.joining(", "));
    }
}
