package pl.app.data.extractor;

import org.apache.commons.lang3.math.NumberUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

class ColumnDataInformationExtractor {

    ColumnDataInformation extractDataColumnInformation(String columnName, List<String> dataList) {
        if (dataList.isEmpty()) {
            throw new IllegalStateException("data.csv contains no data");
        }
        DataType dataType = null;
        for (String data : dataList) {
            DataType currentRecordDataType;
            if (isDate(data)) {
                currentRecordDataType = DataType.DATE;
            } else if (isDouble(data)) {
                currentRecordDataType = DataType.DECIMAL;
            } else if (isInteger(data)) {
                currentRecordDataType = DataType.INTEGER;
            } else {
                currentRecordDataType = DataType.TEXT;
            }

            if (isPreviousRecordNotSameType(dataType, currentRecordDataType)) {
                dataType = DataType.TEXT;
                break;
            }
            dataType = currentRecordDataType;
        }
        return new ColumnDataInformation(columnName, dataType);

    }

    private boolean isPreviousRecordNotSameType(DataType previousRecordType, DataType currentType) {
        return previousRecordType != null && previousRecordType != currentType;
    }

    private boolean isDate(String possibleDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yy", Locale.ENGLISH);
        try {
            LocalDate.parse(possibleDate, formatter);
        } catch (DateTimeParseException e) {
            return false;
        }
        return true;
    }

    private boolean isDouble(String possibleDouble) {
        String decimalPattern = "([0-9]*)\\.([0-9]*)";
        return Pattern.matches(decimalPattern, possibleDouble);
    }

    private boolean isInteger(String possibleInteger) {
        return NumberUtils.isCreatable(possibleInteger);
    }
}