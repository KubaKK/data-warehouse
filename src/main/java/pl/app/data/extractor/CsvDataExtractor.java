package pl.app.data.extractor;

import com.opencsv.CSVReader;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Kuba on 08.09.2020.
 */
public class CsvDataExtractor {

    private static final int HEADER_ROW_INDEX = 0;
    private final ColumnDataInformationExtractor columnDataInformationExtractor = new ColumnDataInformationExtractor();

    public ExtractedData extractData() {
        List<List<String>> records;
        try {
            records = readDataFromCsv();
        } catch (IOException e) {
            throw new IllegalStateException("could not read data.csv");
        }
        if (records.isEmpty()) {
            throw new IllegalStateException("data.csv contains no data");
        }
        List<String> csvHeaderRow = records.get(HEADER_ROW_INDEX);
        records.remove(HEADER_ROW_INDEX);
        List<ColumnDataInformation> columnDataInformation = extractColumnDataInformation(records, csvHeaderRow);
        return new ExtractedData(columnDataInformation, records);
    }

    private List<ColumnDataInformation> extractColumnDataInformation(List<List<String>> records, List<String> csvHeaderRow) {
        List<ColumnDataInformation> columnDataInformation = new ArrayList<>();
        for (int i = 0; i < csvHeaderRow.size(); i++) {
            int iterator = i;
            List<String> data = records.stream().map(row -> row.get(iterator)).collect(Collectors.toList());
            columnDataInformation.add(
                    columnDataInformationExtractor.extractDataColumnInformation(csvHeaderRow.get(i), data));
        }
        return columnDataInformation;
    }

    private List<List<String>> readDataFromCsv() throws IOException {
        List<List<String>> records = new ArrayList<>();
        Resource resource = new ClassPathResource("data.csv");
        try (CSVReader csvReader = new CSVReader(new FileReader(resource.getFile()))) {
            String[] values;
            while ((values = csvReader.readNext()) != null) {
                records.add(Arrays.asList(values));
            }
        }
        return records;
    }

}
