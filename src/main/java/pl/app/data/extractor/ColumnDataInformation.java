package pl.app.data.extractor;

        import org.apache.commons.lang3.builder.EqualsBuilder;
        import org.apache.commons.lang3.builder.HashCodeBuilder;
        import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by Kuba on 08.09.2020.
 */
public class ColumnDataInformation {
    private final String columnName;
    private final DataType dataType;

    public ColumnDataInformation(String columnName, DataType dataType) {
        this.columnName = columnName;
        this.dataType = dataType;
    }

    public String getColumnName() {
        return columnName;
    }

    public DataType getDataType() {
        return dataType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ColumnDataInformation that = (ColumnDataInformation) o;
        return new EqualsBuilder()
                .append(columnName, that.columnName)
                .append(dataType, that.dataType)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(columnName)
                .append(dataType)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("columnName", columnName)
                .append("dataType", dataType)
                .toString();
    }
}
