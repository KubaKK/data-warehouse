package pl.app.data.extractor;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by Kuba on 08.09.2020.
 */
public class ExtractedData {
    private final List<ColumnDataInformation> columnDataInformationList;
    private final List<List<String>> records;

    public ExtractedData(List<ColumnDataInformation> columnDataInformationList, List<List<String>> records) {
        this.columnDataInformationList = columnDataInformationList;
        this.records = records;
    }

    public List<ColumnDataInformation> getColumnDataInformationList() {
        return columnDataInformationList;
    }

    public List<List<String>> getRecords() {
        return records;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExtractedData that = (ExtractedData) o;

        return new EqualsBuilder()
                .append(columnDataInformationList, that.columnDataInformationList)
                .append(records, that.records)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(columnDataInformationList)
                .append(records)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("columnDataInformationList", columnDataInformationList)
                .append("records", records)
                .toString();
    }
}
