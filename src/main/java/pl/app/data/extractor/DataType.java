package pl.app.data.extractor;

/**
 * Created by Kuba on 08.09.2020.
 */
public enum DataType {
    TEXT, INTEGER, DECIMAL, DATE
}
