package pl.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.app.data.query.DataFetchingService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class DataFetchController {

    private final DataFetchingService dataFetchingService;

    @Autowired
    public DataFetchController(DataFetchingService dataFetchingService) {
        this.dataFetchingService = dataFetchingService;
    }

    @GetMapping("/query")
    public List<String> query(@RequestParam(name = "metrics") List<String> metrics,
                            @RequestParam(name = "groupBy", required = false) List<String> groupBy,
                            @RequestParam(name = "filterOn", required = false) List<String> filterOn) {
        return dataFetchingService.fetchData(metrics, Optional.ofNullable(groupBy), Optional.ofNullable(filterOn));
    }


}
